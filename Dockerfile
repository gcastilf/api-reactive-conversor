FROM openjdk:8
ADD target/reactive-conversor.jar reactive-conversor.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "reactive-conversor.jar"]