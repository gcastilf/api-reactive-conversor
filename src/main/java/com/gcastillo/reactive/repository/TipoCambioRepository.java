package com.gcastillo.reactive.repository;

import com.gcastillo.reactive.entity.TipoCambio;
import com.gcastillo.reactive.servicedto.response.TipoCambioResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TipoCambioRepository extends JpaRepository<TipoCambio, String> {
    @Query(value = "FROM TipoCambio tc WHERE (monedadestino_id = ?1  AND monedaorigen_id = ?2 ) OR (monedadestino_id = ?2  AND monedaorigen_id = ?1 )")
    Optional<TipoCambio> findFirstByMonedaOrigenAndMonedaDestino(String monedaOrigenId, String monedaDestinoId);
}
