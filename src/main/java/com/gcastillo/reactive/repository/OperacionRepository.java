package com.gcastillo.reactive.repository;

import com.gcastillo.reactive.entity.Operacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperacionRepository extends JpaRepository<Operacion, String> {
}
