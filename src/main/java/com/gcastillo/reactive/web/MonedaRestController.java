package com.gcastillo.reactive.web;

import com.gcastillo.reactive.service.moneda.MonedaService;
import com.gcastillo.reactive.servicedto.request.AddMonedaRequest;
import com.gcastillo.reactive.servicedto.response.MonedaResponse;
import com.gcastillo.reactive.webdto.request.AddMonedaWebRequest;
import com.gcastillo.reactive.webdto.response.BaseWebResponse;
import com.gcastillo.reactive.webdto.response.MonedaWebResponse;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/moneda")
public class MonedaRestController {

    @Autowired
    private MonedaService monedaService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse>> addMoneda(@RequestBody AddMonedaWebRequest addMonedaWebRequest) {
        return monedaService.addMoneda(toAddMonedaRequest(addMonedaWebRequest))
                .subscribeOn(Schedulers.io())
                .map(s -> ResponseEntity
                        .created(URI.create("/api/moneda/" + s))
                        .body(BaseWebResponse.successNoData()));
    }

    private AddMonedaRequest toAddMonedaRequest(AddMonedaWebRequest addMonedaWebRequest) {
        AddMonedaRequest addMonedaRequest = new AddMonedaRequest();
        BeanUtils.copyProperties(addMonedaWebRequest, addMonedaRequest);
        return addMonedaRequest;
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<List<MonedaWebResponse>>>> getAllMonedas(@RequestParam(value = "limit", defaultValue = "5") int limit,
                                                                                          @RequestParam(value = "page", defaultValue = "0") int page) {
        return monedaService.getAllMonedas(limit, page)
                .subscribeOn(Schedulers.io())
                .map(monedaResponses -> ResponseEntity.ok(BaseWebResponse.successWithData(toMonedaWebResponseList(monedaResponses))));
    }

    private List<MonedaWebResponse> toMonedaWebResponseList(List<MonedaResponse> monedaResponseList) {
        return monedaResponseList
                .stream()
                .map(this::toMonedaWebResponse)
                .collect(Collectors.toList());
    }

    private MonedaWebResponse toMonedaWebResponse(MonedaResponse monedaResponse) {
        MonedaWebResponse monedaWebResponse = new MonedaWebResponse();
        BeanUtils.copyProperties(monedaResponse, monedaWebResponse);
        return monedaWebResponse;
    }

    @DeleteMapping(
            value = "/{monedaId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse>> deleteMoneda(@PathVariable(value = "monedaId") String monedaId) {
        return monedaService.deleteMoneda(monedaId)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
    }

}
