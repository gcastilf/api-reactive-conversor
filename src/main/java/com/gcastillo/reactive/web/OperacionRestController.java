package com.gcastillo.reactive.web;

import com.gcastillo.reactive.service.operacion.OperacionService;
import com.gcastillo.reactive.servicedto.request.AddOperacionRequest;
import com.gcastillo.reactive.servicedto.response.OperacionResponse;
import com.gcastillo.reactive.webdto.request.AddOperacionWebRequest;
import com.gcastillo.reactive.webdto.response.BaseWebResponse;
import com.gcastillo.reactive.webdto.response.OperacionWebResponse;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/operacion")
public class OperacionRestController {

    @Autowired
    private OperacionService operacionService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<OperacionWebResponse>>> addOperacion(@RequestBody AddOperacionWebRequest addOperacionWebRequest) {
        return operacionService.addOperacion(toAddOperacionRequest(addOperacionWebRequest))
                .subscribeOn(Schedulers.io())
                .map(operacionResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(toOperacionWebResponse(operacionResponse))));

    }

    private AddOperacionRequest toAddOperacionRequest(AddOperacionWebRequest addOperacionWebRequest) {
        AddOperacionRequest addOperacionRequest = new AddOperacionRequest();
        BeanUtils.copyProperties(addOperacionWebRequest, addOperacionRequest);
        return addOperacionRequest;
    }

    private OperacionWebResponse toOperacionWebResponse(OperacionResponse operacionResponse) {
        OperacionWebResponse operacionWebResponse = new OperacionWebResponse();
        BeanUtils.copyProperties(operacionResponse, operacionWebResponse);
        return operacionWebResponse;
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<List<OperacionWebResponse>>>> getAllOperaciones(@RequestParam(value = "limit", defaultValue = "5") int limit,
                                                                                             @RequestParam(value = "page", defaultValue = "0") int page) {
        return operacionService.getAllOperaciones(limit, page)
                .subscribeOn(Schedulers.io())
                .map(operacionResponses -> ResponseEntity.ok(BaseWebResponse.successWithData(toOperacionWebResponseList(operacionResponses))));
    }

    private List<OperacionWebResponse> toOperacionWebResponseList(List<OperacionResponse> operacionResponseList) {
        return operacionResponseList
                .stream()
                .map(this::toOperacionWebResponse)
                .collect(Collectors.toList());
    }
}
