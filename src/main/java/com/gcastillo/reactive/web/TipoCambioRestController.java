package com.gcastillo.reactive.web;

import com.gcastillo.reactive.service.tipocambio.TipoCambioService;
import com.gcastillo.reactive.servicedto.request.AddTipoCambioRequest;
import com.gcastillo.reactive.servicedto.response.TipoCambioResponse;
import com.gcastillo.reactive.webdto.response.BaseWebResponse;
import com.gcastillo.reactive.webdto.response.TipoCambioWebResponse;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/tipocambio")
public class TipoCambioRestController {

    @Autowired
    private TipoCambioService tipoCambioService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    ) public Single<ResponseEntity<BaseWebResponse>> addTipoCambio(
            @RequestBody AddTipoCambioRequest addTipoCambioRequest) {
        return tipoCambioService.addTipoCambio(addTipoCambioRequest).subscribeOn(Schedulers.io()).map(
                s -> ResponseEntity.created(URI.create("/api/tipocambio/" + s))
                        .body(BaseWebResponse.successNoData()));
    }

    /*private AddTipoCambioRequest toAddTipoCambioRequest(AddTipoCambioWebRequest addTipoCambioWebRequest) {
        AddTipoCambioRequest addTipoCambioRequest = new AddTipoCambioRequest();
        BeanUtils.copyProperties(addTipoCambioWebRequest, addTipoCambioRequest);
        return addTipoCambioRequest;
    }*/

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<List<TipoCambioWebResponse>>>> getAllTipoCambio(@RequestParam(value = "limit", defaultValue = "5") int limit,
                                                                                                 @RequestParam(value = "page", defaultValue = "0") int page) {
        return tipoCambioService.getAllTipoCambio(limit, page)
                .subscribeOn(Schedulers.io())
                .map(tipocambioResponses -> ResponseEntity.ok(BaseWebResponse.successWithData(toTipoCambioWebResponseList(tipocambioResponses))));
    }

    private List<TipoCambioWebResponse> toTipoCambioWebResponseList(List<TipoCambioResponse> tipoCambioResponseList) {
        return tipoCambioResponseList
                .stream()
                .map(this::toTipoCambioWebResponse)
                .collect(Collectors.toList());
    }

    private TipoCambioWebResponse toTipoCambioWebResponse(TipoCambioResponse tipoCambioResponse) {
        TipoCambioWebResponse tipoCambioWebResponse = new TipoCambioWebResponse();
        BeanUtils.copyProperties(tipoCambioResponse, tipoCambioWebResponse);
        return tipoCambioWebResponse;
    }

    @DeleteMapping(
            value = "/{tipoCambioId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse>> deleteTipoCambio(@PathVariable(value = "tipoCambioId") String tipoCambioId) {
        return tipoCambioService.deleteTipoCambio(tipoCambioId)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
    }

}
