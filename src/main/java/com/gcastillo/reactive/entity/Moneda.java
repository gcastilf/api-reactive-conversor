package com.gcastillo.reactive.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "moneda")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Moneda {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "nombre")
    private String nombre;
}
