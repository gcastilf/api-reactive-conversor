package com.gcastillo.reactive.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "operacion")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Operacion {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "monto")
    private Double monto;

    @Column(name = "montoconvertido")
    private Double montoConvertido;

    @Column(name = "valortipocambio")
    private Double valorTipoCambio;

    @ManyToOne
    @JoinColumn(name = "monedaorigen_id")
    private Moneda monedaOrigen;

    @ManyToOne
    @JoinColumn(name = "monedadestino_id")
    private Moneda monedaDestino;
}
