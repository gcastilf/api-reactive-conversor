package com.gcastillo.reactive.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "tipocambio")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TipoCambio {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "compra")
    private Double compra;

    @Column(name = "venta")
    private Double venta;

    @ManyToOne
    @JoinColumn(name = "monedaorigen_id")
    private Moneda monedaOrigen;

    @ManyToOne
    @JoinColumn(name = "monedadestino_id")
    private Moneda monedaDestino;
}
