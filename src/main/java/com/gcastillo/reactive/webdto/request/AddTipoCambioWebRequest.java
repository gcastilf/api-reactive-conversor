package com.gcastillo.reactive.webdto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddTipoCambioWebRequest {
    private Double compra;
    private Double venta;
    private String monedaOrigenId;
    private String monedaDestinoId;
}
