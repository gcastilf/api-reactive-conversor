package com.gcastillo.reactive.webdto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddOperacionWebRequest {
    private String monedaOrigenId;
    private String monedaDestinoId;
    private Double monto;
}
