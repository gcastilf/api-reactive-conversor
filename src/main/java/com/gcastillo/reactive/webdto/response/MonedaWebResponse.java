package com.gcastillo.reactive.webdto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MonedaWebResponse {
    private String id;
    private String Nombre;
}
