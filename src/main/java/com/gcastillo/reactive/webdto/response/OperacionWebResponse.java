package com.gcastillo.reactive.webdto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OperacionWebResponse {
    private String id;
    private Double monto;
    private Double montoConvertido;
    private Double valorTipoCambio;
    private String monedaOrigenNombre;
    private String monedaDestinoNombre;
}
