package com.gcastillo.reactive.webdto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TipoCambioWebResponse {
    private String id;
    private Double compra;
    private Double venta;
    private String monedaOrigenNombre;
    private String monedaDestinoNombre;
}
