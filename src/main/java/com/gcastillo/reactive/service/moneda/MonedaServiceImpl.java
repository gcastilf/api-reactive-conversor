package com.gcastillo.reactive.service.moneda;

import com.gcastillo.reactive.entity.Moneda;
import com.gcastillo.reactive.repository.MonedaRepository;
import com.gcastillo.reactive.servicedto.request.AddMonedaRequest;
import com.gcastillo.reactive.servicedto.response.MonedaResponse;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MonedaServiceImpl implements MonedaService {

    @Autowired
    private MonedaRepository monedaRepository;

    @Override
    public Single<String> addMoneda(AddMonedaRequest addMonedaRequest) {
        return addMonedaToRepository(addMonedaRequest);
    }

    private Single<String> addMonedaToRepository(AddMonedaRequest addMonedaRequest) {
        return Single.create(singleSubscriber -> {
            String addedMonedaId = monedaRepository.save(toMoneda(addMonedaRequest)).getId();
            singleSubscriber.onSuccess(addedMonedaId);
        });
    }

    private Moneda toMoneda(AddMonedaRequest addMonedaRequest) {
        Moneda moneda = new Moneda();
        BeanUtils.copyProperties(addMonedaRequest, moneda);
        moneda.setId(UUID.randomUUID().toString());
        return moneda;
    }

    @Override
    public Single<List<MonedaResponse>> getAllMonedas(int limit, int page) {
        return findAllMonedasInRepository(limit, page)
                .map(this::toMonedaResponseList);
    }

    private Single<List<Moneda>> findAllMonedasInRepository(int limit, int page) {
        return Single.create(singleSubscriber -> {
            List<Moneda> moneda = monedaRepository.findAll(PageRequest.of(page, limit)).getContent();
            singleSubscriber.onSuccess(moneda);
        });
    }

    private List<MonedaResponse> toMonedaResponseList(List<Moneda> monedaList) {
        return monedaList
                .stream()
                .map(this::toMonedaResponse)
                .collect(Collectors.toList());
    }

    private MonedaResponse toMonedaResponse(Moneda moneda) {
        MonedaResponse monedaResponse = new MonedaResponse();
        BeanUtils.copyProperties(moneda, monedaResponse);
        monedaResponse.setNombre(moneda.getNombre());
        return monedaResponse;
    }

    @Override
    public Completable deleteMoneda(String id) {
        return deleteMonedaInRepository(id);
    }

    private Completable deleteMonedaInRepository(String id) {
        return Completable.create(completableSubscriber -> {
            Optional<Moneda> optionalMoneda = monedaRepository.findById(id);
            if (!optionalMoneda.isPresent())
                completableSubscriber.onError(new EntityNotFoundException());
            else {
                monedaRepository.delete(optionalMoneda.get());
                completableSubscriber.onComplete();
            }
        });
    }
}
