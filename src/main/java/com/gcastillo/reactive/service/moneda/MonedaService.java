package com.gcastillo.reactive.service.moneda;

import com.gcastillo.reactive.servicedto.request.AddMonedaRequest;
import com.gcastillo.reactive.servicedto.response.MonedaResponse;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;

public interface MonedaService {
    Single<String> addMoneda(AddMonedaRequest addMonedaRequest);

    Completable deleteMoneda(String id);

    Single<List<MonedaResponse>> getAllMonedas(int limit, int page);
}
