package com.gcastillo.reactive.service.tipocambio;

import com.gcastillo.reactive.entity.Moneda;
import com.gcastillo.reactive.entity.TipoCambio;
import com.gcastillo.reactive.repository.MonedaRepository;
import com.gcastillo.reactive.repository.TipoCambioRepository;
import com.gcastillo.reactive.servicedto.request.AddTipoCambioRequest;
import com.gcastillo.reactive.servicedto.response.TipoCambioResponse;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TipoCambioServiceImpl implements TipoCambioService {

    @Autowired
    private TipoCambioRepository tipoCambioRepository;
    @Autowired
    private MonedaRepository monedaRepository;

    @Override
    public Single<String> addTipoCambio(AddTipoCambioRequest addTipoCambioRequest) {
        return saveTipoCambioToRepository(addTipoCambioRequest);
    }

    private Single<String> saveTipoCambioToRepository(AddTipoCambioRequest addTipoCambioRequest) {
        return Single.create(singleSubscriber -> {
            Optional<Moneda> optionalMonedaOrigen = monedaRepository.findById(addTipoCambioRequest.getMonedaOrigenId());
            Optional<Moneda> optionalMonedaDestino = monedaRepository.findById(addTipoCambioRequest.getMonedaDestinoId());

            if (!optionalMonedaOrigen.isPresent() || !optionalMonedaDestino.isPresent())
                singleSubscriber.onError(new EntityNotFoundException());
            else {
                String addedTipoCambioId = tipoCambioRepository.save(toTipoCambio(addTipoCambioRequest)).getId();
                singleSubscriber.onSuccess(addedTipoCambioId);
            }
        });
    }

    private TipoCambio toTipoCambio(AddTipoCambioRequest addTipoCambioRequest) {
        TipoCambio tipoCambio = new TipoCambio();
        BeanUtils.copyProperties(addTipoCambioRequest, tipoCambio);
        tipoCambio.setId(UUID.randomUUID().toString());
        tipoCambio.setCompra(addTipoCambioRequest.getCompra());
        tipoCambio.setVenta(addTipoCambioRequest.getVenta());
        tipoCambio.setMonedaOrigen(Moneda.builder()
                .id(addTipoCambioRequest.getMonedaOrigenId())
                .build());
        tipoCambio.setMonedaDestino(Moneda.builder()
                .id(addTipoCambioRequest.getMonedaDestinoId())
                .build());
        return tipoCambio;
    }

    @Override
    public Single<List<TipoCambioResponse>> getAllTipoCambio(int limit, int page) {
        return findAllTipoCambioInRepository(limit, page)
                .map(this::toTipoCambioResponseList);
    }

    private Single<List<TipoCambio>> findAllTipoCambioInRepository(int limit, int page) {
        return Single.create(singleSubscriber -> {
            List<TipoCambio> tipoCambios = tipoCambioRepository.findAll(PageRequest.of(page, limit)).getContent();
            singleSubscriber.onSuccess(tipoCambios);
        });
    }

    private List<TipoCambioResponse> toTipoCambioResponseList(List<TipoCambio> tipoCambioList) {
        return tipoCambioList
                .stream()
                .map(this::toTipoCambioResponse)
                .collect(Collectors.toList());
    }

    private TipoCambioResponse toTipoCambioResponse(TipoCambio tipoCambio) {
        TipoCambioResponse tipoCambioResponse = new TipoCambioResponse();
        BeanUtils.copyProperties(tipoCambio, tipoCambioResponse);
        tipoCambioResponse.setId(tipoCambio.getId());
        tipoCambioResponse.setMonedaOrigenNombre(tipoCambio.getMonedaOrigen().getNombre());
        tipoCambioResponse.setMonedaDestinoNombre(tipoCambio.getMonedaDestino().getNombre());
        tipoCambioResponse.setCompra(tipoCambio.getCompra());
        tipoCambioResponse.setVenta(tipoCambio.getVenta());
        return tipoCambioResponse;
    }

    @Override
    public Completable deleteTipoCambio(String id) {
        return deleteTipoCambioInRepository(id);
    }

    private Completable deleteTipoCambioInRepository(String id) {
        return Completable.create(completableSubscriber -> {
            Optional<TipoCambio> optionalTipoCambio = tipoCambioRepository.findById(id);
            if (!optionalTipoCambio.isPresent())
                completableSubscriber.onError(new EntityNotFoundException());
            else {
                tipoCambioRepository.delete(optionalTipoCambio.get());
                completableSubscriber.onComplete();
            }
        });
    }

    @Override
    public Single<TipoCambioResponse> findByMonedas(String monedaOrigenId, String monedaDestinoId) {
        return findTipoCambioDetailInRepository(monedaOrigenId, monedaDestinoId);
    }

    private Single<TipoCambioResponse> findTipoCambioDetailInRepository(String monedaOrigenId, String monedaDestinoId) {
        return Single.create(singleSubscriber -> {
            Optional<TipoCambio> optionalTipoCambio = tipoCambioRepository
                    .findFirstByMonedaOrigenAndMonedaDestino(monedaOrigenId, monedaDestinoId);

            if (!optionalTipoCambio.isPresent())
                singleSubscriber.onError(new EntityNotFoundException());
            else {
                TipoCambioResponse tipoCambioResponse = toTipoCambioResponse(optionalTipoCambio.get());
                singleSubscriber.onSuccess(tipoCambioResponse);
            }

        });
    }

}
