package com.gcastillo.reactive.service.tipocambio;

import com.gcastillo.reactive.entity.TipoCambio;
import com.gcastillo.reactive.servicedto.request.AddTipoCambioRequest;
import com.gcastillo.reactive.servicedto.response.TipoCambioResponse;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;

public interface TipoCambioService {
    Single<String> addTipoCambio(AddTipoCambioRequest addTipoCambioRequest);

    Single<List<TipoCambioResponse>> getAllTipoCambio(int limit, int page);

    Completable deleteTipoCambio(String id);

    Single<TipoCambioResponse> findByMonedas(String monedaOrigenId, String monedaDestinoId);
}
