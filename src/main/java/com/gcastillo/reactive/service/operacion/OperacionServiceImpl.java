package com.gcastillo.reactive.service.operacion;

import com.gcastillo.reactive.entity.*;
import com.gcastillo.reactive.repository.MonedaRepository;
import com.gcastillo.reactive.repository.OperacionRepository;
import com.gcastillo.reactive.repository.TipoCambioRepository;
import com.gcastillo.reactive.servicedto.request.AddOperacionRequest;
import com.gcastillo.reactive.servicedto.response.OperacionResponse;
import io.reactivex.Single;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OperacionServiceImpl implements OperacionService {

    @Autowired
    private OperacionRepository operacionRepository;

    @Autowired
    private TipoCambioRepository tipoCambioRepository;

    @Autowired
    private MonedaRepository monedaRepository;

    @Override
    public Single<OperacionResponse> addOperacion(AddOperacionRequest addOperacionRequest) {
        return saveOperacionToRepository(addOperacionRequest);
    }

    private Single<OperacionResponse> saveOperacionToRepository(AddOperacionRequest addOperacionRequest) {
        return Single.create(singleSubscriber -> {
            Optional<Moneda> optionalMonedaOrigen = monedaRepository.findById(addOperacionRequest.getMonedaOrigenId());
            Optional<Moneda> optionalMonedaDestino = monedaRepository.findById(addOperacionRequest.getMonedaDestinoId());

            if (!optionalMonedaOrigen.isPresent() || !optionalMonedaDestino.isPresent())
                singleSubscriber.onError(new EntityNotFoundException());
            else {
                Optional<TipoCambio> optionalTipoCambio = getTipoCambioOperacion(addOperacionRequest);

                if(!optionalTipoCambio.isPresent())
                    singleSubscriber.onError(new EntityNotFoundException());
                else {
                    String addedOperacionId = operacionRepository
                            .save(toOperacion(addOperacionRequest,optionalTipoCambio.get())).getId();
                    Optional<Operacion> optionalOperacion = operacionRepository.findById(addedOperacionId);
                    OperacionResponse operacionResponse = toOperacionResponse(optionalOperacion.get());
                    singleSubscriber.onSuccess(operacionResponse);
                }
            }
        });
    }

    private Optional<TipoCambio> getTipoCambioOperacion(AddOperacionRequest addOperacionRequest) {
        return tipoCambioRepository.findFirstByMonedaOrigenAndMonedaDestino(
                addOperacionRequest.getMonedaOrigenId(),
                addOperacionRequest.getMonedaDestinoId());
    }

    private Operacion toOperacion(AddOperacionRequest addOperacionRequest, TipoCambio tipoCambio) {
        Operacion operacion = new Operacion();
        BeanUtils.copyProperties(addOperacionRequest, operacion);
        operacion.setId(UUID.randomUUID().toString());
        operacion.setMonedaOrigen(Moneda.builder()
                .id(addOperacionRequest.getMonedaOrigenId())
                .build());
        operacion.setMonedaDestino(Moneda.builder()
                .id(addOperacionRequest.getMonedaDestinoId())
                .build());
        operacion.setMonto(addOperacionRequest.getMonto());
        if (tipoCambio.getMonedaOrigen().getId().equals(addOperacionRequest.getMonedaOrigenId())) {
            operacion.setValorTipoCambio(tipoCambio.getVenta());
            operacion.setMontoConvertido(addOperacionRequest.getMonto() / tipoCambio.getVenta());
        } else {
            operacion.setValorTipoCambio(tipoCambio.getCompra());
            operacion.setMontoConvertido(addOperacionRequest.getMonto() * tipoCambio.getCompra());
        }
        return operacion;
    }


    @Override
    public Single<List<OperacionResponse>> getAllOperaciones(int limit, int page) {
        return findAllOperacionesInRepository(limit, page)
                .map(this::toOperacionResponseList);
    }

    private Single<List<Operacion>> findAllOperacionesInRepository(int limit, int page) {
        return Single.create(singleSubscriber -> {
            List<Operacion> operacion = operacionRepository.findAll(PageRequest.of(page, limit)).getContent();
            singleSubscriber.onSuccess(operacion);
        });
    }

    private List<OperacionResponse> toOperacionResponseList(List<Operacion> operacionList) {
        return operacionList
                .stream()
                .map(this::toOperacionResponse)
                .collect(Collectors.toList());
    }

    private Single<OperacionResponse> getOperacionDetail(String id) {
        return findOperacionDetailInRepository(id);
    }

    private Single<OperacionResponse> findOperacionDetailInRepository(String id) {
        return Single.create(singleSubscriber -> {
            Optional<Operacion> optionalOperacion = operacionRepository.findById(id);
            if (!optionalOperacion.isPresent())
                singleSubscriber.onError(new EntityNotFoundException());
            else {
                OperacionResponse operacionResponse = toOperacionResponse(optionalOperacion.get());
                singleSubscriber.onSuccess(operacionResponse);
            }
        });
    }

    private OperacionResponse toOperacionResponse(Operacion operacion) {
        OperacionResponse operacionResponse = new OperacionResponse();
        BeanUtils.copyProperties(operacion, operacionResponse);
        operacionResponse.setId(operacion.getId());
        operacionResponse.setMonto(operacion.getMonto());
        operacionResponse.setMontoConvertido(operacion.getMontoConvertido());
        operacionResponse.setValorTipoCambio(operacion.getValorTipoCambio());
        operacionResponse.setMonedaOrigenNombre(operacion.getMonedaOrigen().getNombre());
        operacionResponse.setMonedaDestinoNombre(operacion.getMonedaDestino().getNombre());
        return operacionResponse;
    }

}
