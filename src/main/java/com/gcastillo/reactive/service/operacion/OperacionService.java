package com.gcastillo.reactive.service.operacion;

import com.gcastillo.reactive.servicedto.request.AddOperacionRequest;
import com.gcastillo.reactive.servicedto.response.OperacionResponse;
import io.reactivex.Single;

import java.util.List;

public interface OperacionService {
    Single<OperacionResponse> addOperacion(AddOperacionRequest operationRequest);

    Single<List<OperacionResponse>> getAllOperaciones(int limit, int page);
}
