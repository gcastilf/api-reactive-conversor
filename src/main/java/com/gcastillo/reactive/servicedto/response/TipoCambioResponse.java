package com.gcastillo.reactive.servicedto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TipoCambioResponse {
    private String id;
    private String monedaOrigenNombre;
    private String monedaDestinoNombre;
    private Double compra;
    private Double venta;
}
