package com.gcastillo.reactive.servicedto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OperacionResponse {
    private String id;
    private Double monto;
    private Double montoConvertido;
    private Double valorTipoCambio;
    private String monedaOrigenNombre;
    private String monedaDestinoNombre;
}
