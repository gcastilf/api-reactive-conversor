package com.gcastillo.reactive.servicedto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddOperacionRequest {
    private String monedaOrigenId;
    private String monedaDestinoId;
    private Double monto;
}
