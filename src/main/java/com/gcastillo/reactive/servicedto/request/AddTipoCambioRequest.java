package com.gcastillo.reactive.servicedto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddTipoCambioRequest {
    private String monedaOrigenId;
    private String monedaDestinoId;
    private Double compra;
    private Double venta;
}
